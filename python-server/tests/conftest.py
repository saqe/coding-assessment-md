import requests
import unittest

class MyTest(unittest.TestCase):
    def test_index_page(self):
        response = requests.get("http://localhost:5000/")
        self.assertEqual(response.status_code, 200)

    def test_random_page(self):
        response = requests.get("http://localhost:5000/random")
        # That's a random page will not be present of server and it should give a fail message
        self.assertEqual(response.status_code, 404)

    def test_api_help_link(self):
        response = requests.get("http://localhost:5000/api")
        # Give Green Flag server got the result OK
        self.assertEqual(response.status_code, 200)
        # Should have a header of response is json
        self.assertEqual(response.headers['Content-Type'], 'application/json')

    def test_api_raw_data(self):
        response = requests.get("http://localhost:5000/api/data")
        # Give Green Flag server got the result OK
        self.assertEqual(response.status_code, 200)
        # Should have a header of response is json
        self.assertEqual(response.headers['Content-Type'], 'application/json')

    def test_api_results_data(self):
        response = requests.get("http://localhost:5000/api/result")
        # Give Green Flag server got the result OK
        self.assertEqual(response.status_code, 200)
        # Should have a header of response is json
        self.assertEqual(response.headers['Content-Type'], 'application/json')
        
        json=response.json()
        
        # Response will be always be dict in python not HTML
        self.assertIsInstance(json, dict)
        
        # Both Main keys dataList & ProductByRevenue are present in results
        self.assertIn('dataList', json)
        self.assertIn('ProductByRevenue', json)
        # There should be 4 items in datalist 
        self.assertEqual(len(json['dataList']), 4)


    def test_security_of_server_static_folder(self):
        response = requests.get("http://localhost:5000/static")
        # Server should not show internal folders directly
        self.assertEqual(response.status_code, 404)
    
    def test_security_of_server_static_folder(self):
        # Template folder shouldn't be accessible to anyone on public
        response = requests.get("http://localhost:5000/template")
        # Shouldn't be visible
        self.assertEqual(response.status_code, 404)


    # Security Test of folders

if __name__=='__main__':
    unittest.main()