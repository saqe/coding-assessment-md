from flask import Flask, request, jsonify, render_template
import pandas as pd
import json
from flask_cors import CORS
import os

# Custom Code
from controller.analysis import DataAnalyzer

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api/data/')
def getData():
    df=pd.read_csv('data/data.csv')
    jdata=json.loads(df.to_json(orient='records'))
    return jsonify(jdata)

@app.route('/api/')
def getHelp():
    # Display result in JSON
    with open('data/help.json') as json_file:
        data = json.load(json_file)
    return jsonify(data)

@app.route('/api/result/')
def getResults():
    print(os.getcwd())
    df=pd.read_csv('data/data.csv')
    
    # Analyse the CSV file and 
    analyzer=DataAnalyzer()
    analyzer.load_dataFrame(df)
    
    # Get response after analyzing
    data=analyzer.generateResults()

    # Return JSON Response on page
    return jsonify(data)

if __name__ == '__main__':
    app.run(threaded=True)