import React, { Component } from "react";
class Table extends Component {
  render() {
    return (
      <div className="row justify-content-md-center">
        <div className="col-md-6">
          <div className="card shadow mb-4">
            <div className="card-header py-3">
              <h6 className="m-0 font-weight-bold text-primary">
                Products By Revenue
              </h6>
            </div>
            <div className="card-body">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Product ID</th>
                    <th>Revenue</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.tableProp.map((row) => (
                    <tr key={row.productId}>
                      <td>{row.productId}</td>
                      <td>{row.Revenue}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Table;
