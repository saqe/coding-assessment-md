import React, { Component } from "react";
import Card from "./Card";

class Cards extends Component {
  render() {
    return (
      <div className="row">
        {this.props.cards.map((card) => (
          <Card key={card.name} cardProp={card} />
        ))}
      </div>
    );
  }
}
export default Cards;
