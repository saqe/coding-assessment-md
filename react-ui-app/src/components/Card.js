import React, { Component } from "react";

class Card extends Component {
  state = {
    card: [],
  };
  componentDidMount() {
    this.setState({ card: this.props.cardProp });
  }
  render() {
    const card = this.props.cardProp;
    return (
      <div className="col-xl-3 col-md-6 mb-4">
        <div className="card border-left-primary shadow h-100 py-2">
          <div className="card-body">
            <div className="row no-gutters align-items-center">
              <div className="col mr-2">
                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                  {card.name}
                </div>
                {this.getValue()}
              </div>
              <div className="col-auto">
                <i className="fas fa-server fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  getValue = () => {
    // <span className="h5 mb-0 font-weight-bold text-gray-800 float-left"></span>
    //             <span className="h5 mb-0 font-weight-bold text-gray-800 float-right"></span>

    if (this.state.card.multi) {
      return (
        <React.Fragment>
          <span className="h5 mb-0 font-weight-bold text-gray-800 float-left">
            {this.state.card.values[0].value}
          </span>
          <span className="h5 mb-0 font-weight-bold text-gray-800 float-right">
            {this.state.card.values[1].value}
          </span>
        </React.Fragment>
      );
    } else {
      return (
        <span className="h5 mb-0 font-weight-bold text-gray-800 float-left">
          {this.state.card.value}
        </span>
      );
    }
  };
}

export default Card;
