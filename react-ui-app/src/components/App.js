import React, { Component } from "react";
import Cards from "./Cards";
import Table from "./Table";
import axios from "axios";
class App extends Component {
  state = {
    body: [],
  };
  componentDidMount() {
    axios
      .get("https://assessment-data.herokuapp.com/api/result/")
      .then((res) => this.setState({ body: res.data }));
  }
  render() {
    console.info(this.state.body);
    return <div>{this.checkIfDataExists()}</div>;
  }

  checkIfDataExists = () => {
    if (this.state.body.length === 0) {
    } else {
      return (
        <React.Fragment>
          <Cards cards={this.state.body.dataList} />
          <Table tableProp={this.state.body.ProductByRevenue} />
        </React.Fragment>
      );
    }
  };
}

export default App;
